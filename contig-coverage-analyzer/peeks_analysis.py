import dask.dataframe as dd
import argparse
from dask.distributed import Client, LocalCluster
#client = Client(processes=False)
from dask.diagnostics import ProgressBar
ProgressBar().register()


def is_after(df, trail):
    return (df["base_index"] >= df["start"] + trail)


def is_before(df, trail):
    return (df["base_index"] <= df["end"] - trail)


def coverage_is_below(df, treshold):
    return df["coverage"] < treshold

if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument('bedInput', type=str, help='Bed file containing the coverage information about the contigs')
    argparser.add_argument('--treshold',type=int, default=10)
    args = argparser.parse_args()
    TRESHOLD = args.treshold
    #BED_FILE = 'contigs.coverage1.bed'
    BED_FILE = args.bedInput
    #INDEX_FILE = 'file.contigsv1_sorted.bed'
    dtypes = {"id": "str","base_index" : "uint32", "start" : "uint32","end" : "uint32","coverage" : "uint32"}
    column_names = names=["id","start","end","base_index","coverage"]


    dd_df = dd.read_csv(BED_FILE, sep='\t', dtype=dtypes,usecols=["id","start","end","base_index", "coverage"], names=column_names)


    print("compute_means")

    #def process(s):
    #    return s.sum()
    dd_df.set_index('id')
    #custom_agg = dd.Aggregation('custom_agg', process, lambda s0: s0.sum())
    result = dd_df.where(dd_df['coverage'] > TRESHOLD).groupby(['id'])[['base_index']].aggregate(['min','max']).compute()
    result.columns = ['_'.join(col).strip() if col[1] else col[0] for col in result.columns.values]
    result.reset_index().to_csv('best_fit_{}.tsv'.format(TRESHOLD), sep='\t', index=False)


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 16:13:43 2018

@author: roberta_menafra
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 18:11:19 2018

@author: roberta_menafra
"""

import dask.dataframe as dd
import pandas as pd
import argparse
from dask.diagnostics import ProgressBar
ProgressBar().register()

def is_after(df, trail):
    return (df["base_index"] >= df["start"] + trail)


def is_before(df, trail1):
    return (df["base_index"] <= df["start"] + trail1)



if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument('bedInput', type=str, help='Bed file containing the coverage information about the contigs')
    argparser.add_argument('contigsRegionsFile', type=str, help='file with a list of contigs with boundaries where to search the min')
    argparser.add_argument('--treshold',type=int, default=10)
    argparser.add_argument('--trail', type=int, default=2000)
    argparser.add_argument('--trail1', type=int, default=2000)
    args = argparser.parse_args()
    TRESHOLD = args.treshold
    
    BED_FILE = args.bedInput
    contigsRegionsFile = args.contigsRegionsFile
    dtypes_contigsRegions = {"id" : "str", "trail_left" : "uint32", "trail_right" : "uint32"}
    contigs_df = dd.read_csv(contigsRegionsFile, dtype= dtypes_contigsRegions, sep='\t') #.compute()

    dtypes = {"id": "str", "base_index" : "uint32", "start" : "uint32","end" : "uint32","coverage" : "uint32"}
    column_names = names=["id","start","end","base_index","coverage"]

    dd_df = dd.read_csv(BED_FILE, sep='\t', dtype=dtypes,usecols=["id","start","end","base_index", "coverage"], names=column_names)
    #Set the index of the dataframe to id so that groupby id will be faster
    print('Setting the index on bed input on the id')
    dd_df = dd_df.set_index("id")
    joint_df = dd_df.merge(contigs_df , on='id', how='inner')
    filtered_joint_df = joint_df.where(joint_df['base_index'] >= joint_df['trail_left']).where(joint_df['base_index'] <= joint_df['trail_right']).dropna()
    filtered_joint_df  = filtered_joint_df.set_index('id')
    max_coverage_df = filtered_joint_df.groupby(['id']).aggregate({'coverage' : 'min'}).reset_index() #.set_index('id')
    max_coverage_df = max_coverage_df.rename(columns={'coverage': 'min_coverage'})
    filtered_with_max_df = filtered_joint_df.merge(max_coverage_df, on='id', how='left')
    print("Computing result")
    result_df = filtered_with_max_df.where(filtered_with_max_df['coverage'] == filtered_with_max_df['min_coverage']).dropna()
    result_dtypes ={'start' : "uint32", 'end' : 'uint32', 'base_index' : 'uint32', 'coverage' : 'uint32', 'trail_left' : 'uint32', 'trail_right' : 'uint32', 'min_coverage' : 'uint32'}
    result_df = result_df.groupby('id').aggregate('max').compute().astype(result_dtypes)
    #result_df.to_csv('split_result_points.csv')
    left_parts = result_df[['start', 'base_index']]
    left_parts = left_parts.rename(columns={'base_index' : 'end'})
    left_parts.index = left_parts.index.map(lambda name : name + '-a')
    right_parts = result_df[['base_index', 'end']]
    right_parts = right_parts.rename(columns={'base_index' : 'start'})
    right_parts.index = right_parts.index.map(lambda name : name + '-b')
    pd.concat([left_parts, right_parts]).sort_index().to_csv('split_parts.csv', sep='\t')
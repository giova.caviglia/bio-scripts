import dask.dataframe as dd
import argparse
from dask.diagnostics import ProgressBar
ProgressBar().register()


def is_after(df, trail):
    return (df["base_index"] >= df["start"] + trail)


def is_before(df, trail):
    return (df["base_index"] <= df["end"] - trail)


def coverage_is_below(df, treshold):
    return df["coverage"] < treshold

if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument('bedInput', type=str, help='Bed file containing the coverage information about the contigs')
    argparser.add_argument('contigsInput', type=str, help='Bed file containing the contigs id with their length')
    argparser.add_argument('--trail', type=int, default=2000)
    args = argparser.parse_args()
    TRAIL = args.trail
    #BED_FILE = 'contigs.coverage1.bed'
    BED_FILE = args.bedInput
    #INDEX_FILE = 'file.contigsv1_sorted.bed'
    INDEX_FILE=args.contigsInput
    index_names = dd.read_csv(INDEX_FILE, sep='\t', usecols=[0,2], names=["id", "length"]).compute()
    lengths = index_names['length']
    print("Contigs with length below {} are {}".format(TRAIL*2, lengths.where(index_names['length'] <= TRAIL*2).dropna().count()))

    dtypes = {"base_index" : "uint32", "start" : "uint32","end" : "uint32","coverage" : "uint32"}
    column_names = names=["id","start","end","base_index","coverage"]


    dd_df = dd.read_csv(BED_FILE, sep='\t', dtype=dtypes,usecols=["id","start","end","base_index", "coverage"], names=column_names)
    #Set the index of the dataframe to id so that groupby id will be faster
    dd_df.set_index("id")

    print("compute_means")
    dd_df_central_part = dd_df.where(is_after(dd_df, TRAIL)).where(is_before(dd_df, TRAIL)).dropna()[["id","end","coverage"]].groupby(["id"]).aggregate({"end" : "max", "coverage": ["count","min","max","mean"]}).compute()

    dd_df_central_part.columns = ['_'.join(col).strip() if col[1] else col[0] for col in dd_df_central_part.columns.values]
    dd_df_central_part.to_csv('coverage_stats_{}.tsv'.format(TRAIL), sep="\t")

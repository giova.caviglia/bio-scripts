#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec  1 17:19:21 2018

@author: roberta_menafra
"""
import dask.dataframe as dd
import argparse
from dask.diagnostics import ProgressBar
ProgressBar().register()

def length_is_more_then(df, length):
	return df['end'] > length
	
def is_after(df, trail):
    return (df["base_index"] >= df["start"] + trail)

def is_before(df, trail):
    return (df["base_index"] <= df["end"] - trail)
    
def is_before_abs(df, trail):
    return (df["base_index"] <= trail)    

def is_after_abs(df, trail):
    return (df["base_index"] > trail)

def is_after_left(df, trail):
    return (df["base_index"] > df["end"] - trail)

def coverage_is_below(df, treshold):
    return df["coverage"] < treshold

if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument('bedInput', type=str, help='Bed file containing the coverage information about the contigs')
    argparser.add_argument('shaped_contigsInput', type=str, help='Bed file containing the contigs id with their length')
    argparser.add_argument('--treshold',type=int, default=5)
    argparser.add_argument('--trail', type=int, default=0)
    argparser.add_argument('output_prefix', type=str, help='prefix to name the final output')
    args = argparser.parse_args()
    TRAIL = args.trail
    TRESHOLD = args.treshold
    #BED_FILE = 'contigs.coverage1.bed'
    BED_FILE = args.bedInput
    PREFIX = args.output_prefix
    #SHAPED_FILE = 'contigs.coverage2_shaped_5.tsv'
    SHAPED_FILE=args.shaped_contigsInput
    dtypes_shaped = {"id" : "str", "new_start" : "uint32", "new_end" : "uint32"}
    shaped_contigs_df = dd.read_csv(SHAPED_FILE, dtype= dtypes_shaped, sep='\t', usecols=[0,2,3]).compute()
    dtypes = {"id" : "str", "base_index" : "uint32", "start" : "uint32","end" : "uint32","coverage" : "uint32"}
    column_names = names=["id","start","end","base_index","coverage"]

    dd_df = dd.read_csv(BED_FILE, sep='\t', dtype=dtypes,usecols=["id","start","end","base_index", "coverage"], names=column_names)
    #Set the index of the dataframe to id so that groupby id will be faster
    dd_df.set_index("id")
    joint_df=dd_df.merge(shaped_contigs_df, on='id', how='left')

    print("compute_means")
    joint_df['length']=joint_df['new_end'] - joint_df['new_start']
    joint_filtered_df = joint_df.where(joint_df['base_index']>=joint_df['new_start']+TRAIL).where(joint_df['base_index'] < joint_df['new_end']-TRAIL).dropna(how='all')
    dd_df_central_part = joint_df.where(joint_df['base_index']>=joint_df['new_start']+TRAIL).where(joint_df['base_index'] < joint_df['new_end']-TRAIL).where(coverage_is_below(joint_df, TRESHOLD)).dropna(how='all')[["id","end","length","coverage"]].groupby(["id"]).aggregate({"end" : "max", "length": "max", "coverage": "count"}).compute()
    
    #dd_df_left_end = dd_df.where(is_after_abs(dd_df, 1000)).where(is_before_abs(dd_df, TRAIL)).where(length_is_more_then(dd_df, LENGTH)).where(coverage_is_below(dd_df, TRESHOLD)).dropna()[["id","end","coverage"]].groupby(["id"]).aggregate({"end" : "max", "coverage": "count"}).compute()
    #dd_df_right_end = dd_df.where(is_before(dd_df, 1000)).where(is_after_left(dd_df, TRAIL)).where(length_is_more_then(dd_df, LENGTH)).where(coverage_is_below(dd_df, TRESHOLD)).dropna()[["id","end","coverage"]].groupby(["id"]).aggregate({"end" : "max", "coverage": "count"}).compute()
    dd_df_central_part["percentage"] = dd_df_central_part["coverage"]/(dd_df_central_part["length"] - TRAIL*2)
    dtypes_joint = {"new_start" : "uint32", "new_end" : "uint32","base_index" : "uint32","coverage":"uint32"}
    joint_filtered_df[['id','new_start','new_end','base_index','coverage']].compute().astype(dtypes_joint).to_csv('filtered{}treshold.csv'.format(TRESHOLD), sep="\t", index=False, header=False)
    dtypes_result = {"end" : "uint32", "length" : "uint32","coverage" : "uint32"}
    dd_df_central_part.astype(dtypes_result).to_csv('coverage_{}_treshold_{}_{}.tsv'.format(PREFIX, TRAIL, TRESHOLD), sep="\t")
    #dd_df_left_end["percentage"] = dd_df_left_end["coverage"]/(TRAIL - 1000)
    #dd_df_left_end.to_csv('left_coverage_{}_treshold_{}_{}.tsv'.format(PREFIX, TRAIL, TRESHOLD), sep="\t")
    #dd_df_right_end["percentage"] = dd_df_right_end["coverage"]/(TRAIL - 1000)
    #dd_df_right_end.to_csv(right_coverage_{}_treshold_{}_{}.tsv'.format(PREFIX, TRAIL, TRESHOLD), sep="\t")
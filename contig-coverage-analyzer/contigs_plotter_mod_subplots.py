#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 18:11:19 2018

@author: roberta_menafra
"""
import matplotlib 
matplotlib.use('PDF')
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
import dask.dataframe as dd
import argparse
from dask.diagnostics import ProgressBar
ProgressBar().register()

if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument('bedInput', type=str, help='Bed file containing the coverage information about the contigs')
    argparser.add_argument('contigsId', type=str, help='Bed file containing the contigs id with their length')
    argparser.add_argument('output', type=str, help='Pdf file containing the final output')
    argparser.add_argument('--treshold',type=int, default=10)
    argparser.add_argument('--trail', type=int, default=2000)
  
    args = argparser.parse_args()
    TRAIL = args.trail
    TRESHOLD = args.treshold

    #BED_FILE = 'contigs.coverage1.bed'
    BED_FILE = args.bedInput
    #INDEX_FILE = 'file.contigsv1_sorted.bed'
    ContigID=args.contigsId
    output=args.output
    dtypes = {"base_index" : "uint32", "start" : "uint32","end" : "uint32","coverage" : "uint32"}
    column_names = names=["id","start","end","base_index","coverage"]

    dd_index = dd.read_csv(ContigID, sep='\t', dtype=dtypes, usecols=["id"]).compute()
    dd_df = dd.read_csv(BED_FILE, sep='\t', dtype=dtypes,usecols=["id","start","end","base_index", "coverage"], names=column_names)
    #Set the index of the dataframe to id so that groupby id will be faster
    dd_df.set_index("id")
    #Calculate the number of subplots and rows for the final figure
    number_of_subplots=len(dd_index.index)
    #number of columns is fix to 3
    columns=3
    rows = number_of_subplots // columns
    if (number_of_subplots % columns != 0):
        rows +=1
    position = range(1, number_of_subplots + 1)
    #figure dimensions are scaled on what I know would fit well a 14 rows x 3columns pdf
    fig = plt.figure(figsize=(20,40/14*rows))
    for k in position:
        i = dd_index['id'].iloc[k-1]
        print("Computing data to plot for {}".format(i))
        contig_data = dd_df.groupby(["id"]).get_group(i).compute()
        mean=contig_data['coverage'].mean()
        x =contig_data['base_index']
        y =contig_data['coverage']
        
        #ax.set_ylim(0.5, y.max())
        ax = fig.add_subplot(rows, columns, k) 
        ax.set_xlim(0, x.max()*1.01)
        ax.plot(x, y, linewidth=1)
        
        ax.set_title('{}'.format(i))
        ax.set_xlabel('length')        
        ax.set_ylabel('coverage')
        ax.axhline(TRESHOLD,linewidth=1, color='r', alpha = 0.7, ls='--')
        ax.axhline(mean,linewidth=1, color='b', alpha = 0.4, ls='--')
        ax.axhspan(0,mean/5, color='r', alpha = 0.4)
        ax.axvline(x.max(), linewidth=1, color='r', alpha = 0.7, ls='--')
        #ax.axvline(contig_data['end'].iloc[0]-(TRAIL),linewidth=1, color='r', alpha = 0.7, ls='--')
        ax.tick_params(axis='x',) 
        ax.ticklabel_format(axis='x',style = 'sci', scilimits=(0,0))
    plt.tight_layout()    
    plt.savefig(output, format='pdf')
    plt.close()
    
import os
import re
import pandas as pd

WORK_DIR = "test-data/"

# example to match: HJ3LKDSXX_103638-001-001-001_CAGATCAT-CGTACTGA_L004_R1.fastq.gz
FILE_PATTERN =r"(HJ3LKDSXX_103638-\d{3}-\d{3}-\d{3}_)([CATG]*-[CATG]*)(_.*\.fastq\.gz)"

df = pd.read_csv('Book1.csv')
df['combined'] = df[['index1', 'index2']].apply("-".join, axis=1)
df = df.set_index('combined')
#dic = { "cane" : "bau", "gatto" : "miao"}
dic = df['ID']

if os.path.isdir(WORK_DIR):
    for filename in os.listdir(path=WORK_DIR):
        match = re.match(FILE_PATTERN, filename)
        if match:
            found_couple = match.group(2)
            if found_couple in dic.index:
                new_name = re.sub(FILE_PATTERN, r"\1" + dic.loc[found_couple] + r"\3", filename)
                print("Replacing", filename, "with", new_name)
                os.rename(os.path.join(WORK_DIR, filename), os.path.join(WORK_DIR, new_name))
            else:
                print("Problem in file", filename, ":", found_couple, "not found in reference.")
        else:
            print("Problem in file", filename, ":", "file name does not match the pattern.")

else:
    print(WORK_DIR, "is not a vaild directory or does not exist.")

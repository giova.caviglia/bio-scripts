#!/bin/bash
# Group v-genes (5-th columns) of the consensus file (passed as first argument) by clonotype
# return a csv with two columns (clonotype_id,v-genes list) 
awk -F ',' 'NR>1{
   if (arr[$1])
     arr[$1] = arr[$1]"|"$5
   else
     arr[$1] = $5
  } 
  END {
    for (a in arr) {
      print a","arr[a]
    }
  }' $1 

sort -u $1 | while read line; 
do
 matches=$(cat $2 | grep $line | sed -e 's/^/'$line',/');
 count=$(echo "$matches" | awk 'NF' | wc -l);
 if (( $count > 0 )); 
  then
   echo "$matches";
  fi
done

# summary.sh consensus_annotations.csv clonotypes.csv mutated_names 
./group.sh $1 | sort -t ',' -k 1,1 > clonotype_matched.csv
awk -F ',' '{print $1","$2}' $2 | sort -t ',' -k 1,1 > sorted_clonotypes.csv
join -t ',' sorted_clonotypes.csv clonotype_matched.csv | sort -t ',' -rnk 2,2 > clonotype_matched_with_frequency.csv
./matched.sh $3 clonotype_matched_with_frequency.csv > final_result.csv

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 07:16:35 2018

@author: roberta_menafra
"""

import sys 
import re
from Bio import SeqIO
from Bio.SeqIO.QualityIO import FastqGeneralIterator

def toFastq(title, sequence, quality):
    return "@{}\n{}\n+\n{}\n".format(title, sequence, quality)

def indexesFinder(header, index1, index2):
    return re.search(index1 + "[ATGC]{2}\+" + index2 + "[ATGC]{2}$", header) != None

def readIndexes(file):
    with open(file) as f:
        content = f.readlines()[1:]
        return [x.strip().split(',') for x in content]
    
def filterFastq(input_file, output_file, couples):
    for couple in couples:
     with open(couple[0] + output_file, "w") as output:
        with open(input_file, "rU") as inputHandle:
          for (title, sequence, quality) in FastqGeneralIterator(inputHandle):
            if indexesFinder(title, couple[1], couple[2]):
               output.write(toFastq(title, sequence, quality))
    
def main(fasR1, fasR2, indexesFile):
   couples = readIndexes(indexesFile) 
   filterFastq(fasR1, '_R1.fastq', couples)
   filterFastq(fasR1, '_R2.fastq', couples)

if __name__=='__main__':       
   main(sys.argv[1], sys.argv[2], sys.argv[3])       

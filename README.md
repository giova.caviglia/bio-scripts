# Contigs coverage analyzer

Contigs coverage analyzer is a set of python scripts to inspect the coverage distribution over the contigs obtained from a de-novo assembly project (after the polishing step).

Contigs coverage (number of reads covering a specific position) generally increases from the edges to the middle part of a contig. 
We want to trim the extremities of the contigs based on coverage (value below 5), but also to inspect if there is a sudden drop in coverage in the middle of the sequence. 
This can be a sign of two pieces of sequences assembled together but not belonging to the same region.

We calculate for all contigs the coverage over three regions: 1kb after the start, 1kb before the end and in the remaining central part. 
The logic behind is that low coverage at the extremities can be tolerated, but in the central region it shouldn't!

## Usage

```
Usage: python coverage_treshold_mod.py [OPTIONS]

Options:
  --treshold  INTEGER      coverage treshold (default 5)
  --trail     INTEGER      distance from start and end of the sequence to consider as extremeties of the contig default is 2000
  --length    INTEGER      minimum contig length to consider, default is 4000
  --coverage_bed  STR      bed file containing basepair coverage values
  --contigs_bed   STR      bed file containing the contigs id with their length
```
```
Usage: python coverage_stats.py [OPTIONS]

  --trail     INTEGER      distance from start and end of the sequence to consider as extremeties of the contig default is 2000
  --coverage_bed  STR      bed file containing basepair coverage values
  --contigs_bed   STR      bed file containing the contigs id with their length
  
  Output is a tsv file containing 6 columns: contig_id, length, number of bases, min_coverage, max_coverage, mean_coverage 
```
A different approach instead of giving a fix trail is to remove only bases at the extremities when their coverage goes under the threshold.
coverage_treshold_2.py takes as input the original coverage bed file and the tsv output file of shape analysis and it returns the contigs whose coverage goes below the treshold
```
Usage: python shape_analysis.py bedInput outputFile [OPTIONS]

  bedInput     STR      name of the bed coverage file
  --treshold   INT      coverage treshold, default is 10
  outputFile   STR      name of the tsv output file     
  
  Output is a tsv file containing 6 columns: 
```
``` 
Usage: 
coverage_treshold_2.py bedInput trimmedFile outputFile
--treshold   INT      coverage treshold, default is 5
bedInput     STR      name of the bed coverage file
shapedfile   STR      name of the tsv file from shape.analysis.py
output       STR      prefix of the output file

By default another output generated is a tsv containing basepair coverage of all contigs from bedInput with the new start and end coordinates from shape_analysis.py
```
```
Usage: 
coverage_stats2.py bedInput trimmedFile outputFile
bedInput     STR      name of the bed coverage file
shapedfile   STR      name of the tsv file from shape.analysis.py
output       STR      prefix of the output file
Output file contains statistics (coverage min, coverage max, coverage mean) on the original bedfile with base pair coverage grouping the stats by contigsID
```
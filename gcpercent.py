# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import sys 
from Bio import SeqIO

def getGC(seq):
    return(sum([1.0 for nucl in seq if nucl in ['G', 'C']]) / len(seq) * 100.0)
def processFasta(fas):
    print("identifier\tGCcontent\tlength")
    for record in SeqIO.parse(fas, "fasta"):
        print("{}\t{:.2f}\t{}".format(record.id, getGC(str(record.seq)), str(len(record.seq))))
        
processFasta(sys.argv[1])
